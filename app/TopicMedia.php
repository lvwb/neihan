<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicMedia extends Model
{
    protected $table = 'topic_media';
    protected $primaryKey = 'me_id';

    public function topicInfo()
    {
        return $this->belongsTo(TopicInfo::class,'topic_id','topic_id');
    }
}
