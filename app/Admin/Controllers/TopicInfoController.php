<?php

namespace App\Admin\Controllers;

use App\TopicInfo;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TopicInfoController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TopicInfo::class, function (Grid $grid) {

            $grid->topic_id('topic_id')->sortable();

            $grid->text('内容');
            // 设置text、color、和存储值
            $states = [
                'on'  => ['value' => 102, 'text' => '打开', 'color' => 'primary'],
                'off' => ['value' => 101, 'text' => '关闭', 'color' => 'default'],
            ];
            $grid->media_type('类型')->select([
                0 => '文字',
                1 => '图片',
                2 => 'GIF',
                3 => '视频',
            ]);
            //$grid->column('topic_media.mp4_url');

            $grid->topicMedia()->mp4_url();
            $grid->status('状态')->switch($states);

            $grid->create_time('创建时间')->display(function($date) {
                return date('Y-m-d H:i:s',$date);
            });
            ;
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TopicInfo::class, function (Form $form) {

            $form->text('topic_id', 'ID');

            $form->text('text', '内容');
            // 设置text、color、和存储值
            $states = [
                'on'  => ['value' => 1, 'text' => '打开', 'color' => 'primary'],
                'off' => ['value' => 2, 'text' => '关闭', 'color' => 'default'],
            ];
            $options = [ 0 => '文字',
                1 => '图片',
                2 => 'GIF',
                3 => '视频'];
            $form->select('media_type','类型')->options($options);
            $form->switch('status','状态')->states($states);
           // $form->display('create_time', '创建时间');
        });
    }
}
