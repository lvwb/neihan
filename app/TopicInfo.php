<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicInfo extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'topic_info';
    protected $primaryKey = 'topic_id';


    /**
     * 默认使用时间戳戳功能
     *
     * @var bool
     */
    public $timestamps = true;
    const CREATED_AT = 'create_time';




    /**
     * 从数据库获取的为获取时间戳格式
     *
     * @return string
     */
    public function getDateFormat() {
        return 'U';
    }


    protected $fillable = [
        'name', 'email', 'password','status',
    ];

    /**
     * 获得关联的媒体信息。
     */
    public function topicMedia()
    {
        return $this->hasOne(TopicMedia::class);
    }

}
